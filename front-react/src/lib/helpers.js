/* eslint-disable */
import moment from 'moment';
import { categoryOptions } from '../constants';
import { formatTime } from './stylish';


export const getCategoryName = function(category) {
	return categoryOptions.find((element) => element.key === category).text;
}

export const getEndTime = function(date, length) {
	var endDate = moment(date).add(Math.floor(length/60), 'h').add(length%60, 'm').toDate();
	return formatTime(endDate);
}

export const getFullTime = function(date, length) {
	return `${formatTime(new Date(date))} - ${getEndTime(date, length)}`;
}

export const getAgeRestriction = function(age) {
	let message = "";
	if(typeof age.min !== "undefined" && typeof age.max !== "undefined") {
		message = `Esta actividad es para personas entre ${age.min} y ${age.max} años`;
	} else if(typeof age.min !== "undefined"  && typeof age.max === "undefined") {
		message = `Para esta actividad debes ser mayor de ${age.min} años`;
	} else if(typeof age.min === "undefined"  && typeof age.max !== "undefined") {
		message = `Para esta actividad debes ser menor de ${age.man} años`;
	} else {
		message = `Esta actividad es para personas de todas las edades`;
	}
	return message;
}

export const getQueryParam = function(query, paramName) {
	let parameters = {};
	let array = query.replace('?','').split('&');

	array.map(element => parameters[element.split('=')[0]] = element.split('=')[1]);
	return parameters[paramName];
}

export const filterFuture = (sessions) => {
	const today = new Date();
	let result = [];
	for(var session in sessions) {
		let date = new Date(sessions[session].date);
		if(today<date) {
			sessions[session].key = session;
			result.push(sessions[session]);
		}
	}
	return result;
}

export const getAge = function(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

export const getMonthLimit = (month, year) => {
	var limit = 31;

	if(month != '') {
		if(month == 2) { // february
			if(year % 4 == 0) limit = 29;
			else limit = 28;
		}
		else { // rest of the year
			if(month < 8) {
				if(month % 2 == 0) limit = 30;
			} else {
				if(month % 2 == 1) limit = 30;
			}
		}
	}

	return limit;
}

export const getDayOptions = (month, year) => {
	var days = [];
	var limit = getMonthLimit(month, year);

	for(var d = 1; d <= limit; d++) {
		days.push({
			key: d,
			value: d,
			text: d.toString()
		})
	}
	return days;
}
