import moment from 'moment';


const shortMonths = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
  "Septiembre", "Octubre", "Noviembre", "Diciembre"]
const dayNames = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]


export const formatTime = function(date) {
  return moment(date).format("hh:mma");
}

export const formatLength = function(minutes) {
  return Math.floor(minutes/60) + "h " + minutes%60 + "m";
}

export const getMonthName = function(date) {
  return monthNames[date.getMonth()];
}

export const getDayName = function(date) {
  return dayNames[date.getDay()];
}

export const roundMinutes = function(date) {
  date.setHours(date.getHours() + Math.round(date.getMinutes()/60));
  date.setMinutes(0);

  return date;
}

export const shortenMonth = function(date) {
  return shortMonths[date.getMonth() + 1];
}

export const styleDate = function(date, includeYear=false) {
  if(typeof date === "number")  date = new Date(date);
  return date.getDate() + ' de ' + getMonthName(date) + (includeYear ? ' del ' + date.getFullYear() : '');
}

export const styleDate2 = function(date, includeYear=false) {
  if(typeof date === "number")  date = new Date(date);
  return getDayName(date) + ", " + styleDate(date);
}

/**
 * Renders a given date into a "DD Mon" fashion. Example: 01 Jan.
 * @param {date} date - The date to be styled.
 */
export const styleShortDate = function(date) {
  if(typeof date === "number")  date = new Date(date);
  return date.getDate() + " " + shortenMonth(date);
}

export const stylePrice = function(price) {
  if (price === 0) {
    return 'Gratis'
  } else {
    var m = Math.trunc(price / 1000);
    var u = (price - (m * 1000)).toString();
    if(u.length === 2) {
      u = '0' + price;
    }
    if(u.length === 1) {
      u = '00' + u;
    }
    return 'COP $' + m + '.' + u;
  }
}

export const styleDatetime = function(date) {
  return dayNames[date.getDay()] + " " + styleDate(date) + " a las " + formatTime(date);

}
