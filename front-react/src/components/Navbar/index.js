/* eslint-disable */
import React, { Component } from 'react';
import { LOGO } from '../../constants.js';
import { Message } from 'semantic-ui-react';

import './styles.css';

export default class Navbar extends Component {
  constructor() {
    super();

    this.state = {  };
  }

	componentWillMount() {
	}

  render() {
		return (
			<div id="navbar" className="ui secondary inverted menu">
				<a href="/" className="item"><img id="logo" src={LOGO} alt="Iceberg logo" className="ui small image"/></a>

				<label id="menu-label" className="item" htmlFor="menu-checkbox"><i className="big bars icon"></i></label>
				<input type="checkbox" id="menu-checkbox" />

        <div className="right menu">
  				<a className="item" href="/#services"><span>Servicios</span></a>
  				<a className="item" href="/#howWorks">Cómo lo hacemos</a>
  				<a className="item" href="/#contact">Contáctanos</a>
  			</div>
			</div>
		);
  }
}
