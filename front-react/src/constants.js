export const LOGO = "/logo.png";

export const monthOptions = [
	{ key: 1, value: 1, text: 'Enero' },
	{ key: 2, value: 2, text: 'Febrero' },
	{ key: 3, value: 3, text: 'Marzo' },
	{ key: 4, value: 4, text: 'Abril' },
	{ key: 5, value: 5, text: 'Mayo' },
	{ key: 6, value: 6, text: 'Junio' },
	{ key: 7, value: 7, text: 'Julio' },
	{ key: 8, value: 8, text: 'Agosto' },
	{ key: 9, value: 9, text: 'Septiembre' },
	{ key: 10, value: 10, text: 'Octubre' },
	{ key: 11, value: 11, text: 'Noviembre' },
	{ key: 12, value: 12, text: 'Diciembre' },
]

export const yearOptions = (() => {
	var years = [];
	var thisYear = (new Date()).getFullYear();
	for(var y = thisYear - 10; y > thisYear - 90; y--) {
		years.push({
			key: y,
			value: y,
			text: y.toString()
		})
	}
	return years;
})()

export const categoryOptions = [
	{ label: 'Danza', text: 'Danza', value: 'dance', key: 'dance' },
	{ label: 'Comida', text: 'Comida', value: 'food', key: 'food' },
	{ label: 'Aire libre', text: 'Aire libre', value: 'outdoors', key: 'outdoors' },
	{ label: 'Fotografía', text: 'Fotografia', value: 'photography', key: 'photography' },
	{ label: 'DIY', text: 'DIY', value: 'diy', key: 'diy' },
	{ label: 'Deporte', text: 'Deporte', value: 'sport', key: 'sport' }
];

export const priceOptions = [
	{ label: 'Menos de $50.000', value: '50000', key: '1' },
	{ label: '$50.000 a $75.000', value: '75000', key: '2' },
	{ label: '$75.000 a $100.000', value: '100000', key: '3' },
	{ label: 'Mas de $100.000', value: '100000000', key: '4' },
];

export const languageOptions = [
	{ key: 'EN', value: 'EN', text: 'Inglés' },
	{ key: 'FR', value: 'FR', text: 'Francés' },
	{ key: 'DE', value: 'DE', text: 'Alemán' },
	{ key: 'PT', value: 'PT', text: 'Portugués' },
]
