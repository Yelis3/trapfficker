/* eslint-disable */
import React, { Component } from 'react';
import { Loader } from 'semantic-ui-react';
import './styles.css';

export default class Home extends Component {
	constructor() {
		super();

		this.state = {
		};

	}

	componentWillMount() {
	}

  render() {
    return (
			<div>
				<img class="intro" src="images/intro.jpeg"></img>
				<div className="ui compact page">
					<center>
						<div className="services">
							<a id="services"></a>
							<h2>¿Qué ofrecemos?</h2>
							<p className="blue">Todo lo que tenemos para ayudarle a usted, <b>como entidad pública, a luchar contra el anonimato</b> de la explotación sexual en internet</p>
							<div className="ui four column grid">
								<a href="/dictionary" className="column">
									<img src="images/info/services/Diccionario.png"></img>
									<p><b>Diccionario</b> y generación de <b>palabras clave</b></p>
								</a>
								<a href="/content" className="column">
									<img src="images/info/services/Contenido.png"></img>
									<p>Reconocimiento de <b>Contenido</b></p>
								</a>
								<a href="/consumer" className="column">
									<img src="images/info/services/Consumidor.png"></img>
									<p>Reconocimiento de <b>Comunidades consumidoras</b></p>
								</a>
								<a href="/visualize" className="column">
									<img src="images/info/services/Database.png"></img>
									<p><b>Visualización</b> de datos</p>
								</a>
							</div>
						</div>

						<div class="ui divider"></div>

						<div className="howWorks">
							<a id="howWorks"></a>
							<h2>¿Cómo funciona?</h2><br/>
							<img src="images/info/how-works.png"></img>
						</div>

					</center>
				</div>
			</div>
    )
  }
}
