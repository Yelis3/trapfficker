/* eslint-disable */
import React, { Component } from 'react';
import { Loader, Form } from 'semantic-ui-react';
import './styles.css';

export default class Home extends Component {
	constructor() {
		super();

		this.state = {
			data: ''
		};
		this.onSubmit = this.onSubmit.bind(this);

	}

	componentWillMount() {
	}

	onSubmit(event) {
		console.log(event);
		fetch('http://localhost:3001/api/content', {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		    url: 'https://www.google.com'
		  })
		})
		.then(response => response.json())
		.then(responseJson => {
			this.setState({
				data: responseJson
			})
		})
		console.log(this.state.data);
	}

  render() {
    return (
			<div className="ui compact page">
				<Form onSubmit={this.onSubmit}>
					<div class="ui icon input">
						<i class="search icon"></i>
						<input type="text" placeholder="www.ejemplo.com" />
						<Form.Button color="blue">Analizar</Form.Button>
					</div>
				</Form>
			</div>
    )
  }
}
