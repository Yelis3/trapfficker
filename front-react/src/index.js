import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Router, Redirect } from 'react-router-dom';

import Navbar from './components/Navbar';
import Footer from './components/Footer';

import Home from './scenes/Home';
import Dictionary from './scenes/Dictionary';
import Content from './scenes/Content';
import NotFound from './scenes/NotFound';

import './styles/index.css';
import './styles/page.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
	<BrowserRouter>
		<div id="app">
			<Navbar />
			<main>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/dictionary" component={Dictionary} />
					<Route exact path="/content" component={Content} />
					<Route exact path="/404" component={NotFound} />
					<Redirect to="/404" />
				</Switch>
			</main>
			<Footer />
		</div>
	</BrowserRouter>
	), document.getElementById('root'));
registerServiceWorker();
