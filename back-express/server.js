'use strict';
const port = process.env.PORT || 3001;
var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var words = require('./data/dictionary').words;
var ctrl = require('./controllers');


// app.use(cors({ origin: true }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.listen(port);


// Routes
app.get('/api/word/:word', ctrl.wordsController);
app.post('/api/content', ctrl.urlController);
app.post('/api/content/percentage', ctrl.percentage);
