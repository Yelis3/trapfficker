var https = require('https');
var fs = require('fs');
var NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');

const nlu = new NaturalLanguageUnderstandingV1({
  username: '78a44042-7f55-4030-accc-7087879aa3e3',
  password: 'x8RPo2gEN4SL',
  version_date: '2018-05-05',
  url: 'https://gateway.watsonplatform.net/natural-language-understanding/api/'
});


module.exports = {
  wordsController: function (req, res) {
    if(words[req.param.word]){
      res.json(words[req.param.word]);
    }else{
      res.json([req.param.word]);
    }
  },

  urlController: function (req, response) {
    var url = req.body.url;

    // get html content
    https.get(url, (resp) => {
      var data = '';
      resp.on('data', (chunk) => {
        data += chunk;
      });
      resp.on('end', () => {
        // get watson analysis
        console.log('HTML content: ', data);
        var options = {
          html: data ,
          features: {
            concepts: {},
            keywords: {}
          }
        };

        nlu.analyze(options, function(err, res) {
          if (err) {
            console.log('Error in watson analysis', err);
            return;
          }
          response.send(res);
        });
      });
    }).on("error", (err) => {
      console.log("Error in http request: " + err.message);
    });
  },

  percentage: function (req, res) {
    res.send(req.body);
  }
}
